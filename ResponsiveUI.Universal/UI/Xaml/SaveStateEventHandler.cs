﻿namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Represents the method that will handle the <see cref="NavigationService.SaveState" />event
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="SaveStateEventArgs"/> instance containing the event data.</param>
    public delegate void SaveStateEventHandler(object sender, SaveStateEventArgs e);
}