﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Provides methods for working with Views.
    /// </summary>
    public static partial class View
    {
        /// <summary>
        /// Loads the state.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="navigationParameter">The navigation parameter.</param>
        /// <param name="pageState">State of the page.</param>
        public static void LoadState(Page page, object navigationParameter, IDictionary<string, object> pageState)
        {
        }
    }
}
