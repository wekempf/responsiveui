﻿namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Represents the method that will handle the <see cref="NavigationService.LoadState" />event
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="LoadStateEventArgs"/> instance containing the event data.</param>
    public delegate void LoadStateEventHandler(object sender, LoadStateEventArgs e);
}