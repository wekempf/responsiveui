﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Responsive.Threading;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Encapsulates the responsive application and its available services.
    /// </summary>
    public class ResponsiveApplication : Application, IResponsiveApplication, IDisposable
    {
        private readonly AsyncCoordinator asyncCoordinator = new AsyncCoordinator();
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponsiveApplication"/> class.
        /// </summary>
        public ResponsiveApplication()
        {
            this.Suspending += this.ResponsiveApplication_Suspending;
        }

        /// <summary>
        /// Gets a <see cref="CancellationToken" /> that can be used to observe when the <see cref="AsyncCoordinator" />
        /// is shutdown.
        /// </summary>
        public CancellationToken CancellationToken
        {
            get { return this.asyncCoordinator.CancellationToken; }
        }

        /// <summary>
        /// Adds a <see cref="Task" /> to be waited on when this <see cref="AsyncCoordinator" /> is shutdown.
        /// </summary>
        /// <param name="task">The task to add.</param>
        public void AddTask(Task task)
        {
            this.asyncCoordinator.AddTask(task);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            if (!this.disposed)
            {
                this.asyncCoordinator.Dispose();
                this.disposed = true;
            }
        }

        /// <summary>
        /// Requests that the shutdown operation be delayed.
        /// </summary>
        /// <returns>
        /// The operation deferral.
        /// </returns>
        public Deferral GetDeferral()
        {
            return this.asyncCoordinator.GetDeferral();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        protected virtual void Initialize()
        {
        }

        /// <summary>
        /// Invoked when the application is activated through file-open.
        /// </summary>
        /// <param name="args">Event data for the event.</param>
        protected override void OnFileActivated(FileActivatedEventArgs args)
        {
            base.OnFileActivated(args);
            this.Initialize();
        }

        /// <summary>
        /// Invoked when the application is launched. Override this method to perform application initialization and to display initial content in the associated Window.
        /// </summary>
        /// <param name="args">Event data for the event.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            base.OnLaunched(args);
            this.Initialize();
        }

        /// <summary>
        /// Invoked when the application is activated through a search association.
        /// </summary>
        /// <param name="args">Event data for the event.</param>
        protected override void OnSearchActivated(SearchActivatedEventArgs args)
        {
            base.OnSearchActivated(args);
            this.Initialize();
        }

        /// <summary>
        /// Invoked when the application is activated through sharing association.
        /// </summary>
        /// <param name="args">Event data for the event.</param>
        protected override void OnShareTargetActivated(ShareTargetActivatedEventArgs args)
        {
            base.OnShareTargetActivated(args);
            this.Initialize();
        }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(this.asyncCoordinator != null);
        }

        private async void ResponsiveApplication_Suspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            try
            {
                await this.asyncCoordinator.ShutdownAsync();
            }
            finally
            {
                deferral.Complete();
            }
        }
    }
}