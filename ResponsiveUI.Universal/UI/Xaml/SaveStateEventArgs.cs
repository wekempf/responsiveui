﻿using System;
using System.Collections.Generic;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Class used to hold the event data required when a page attempts to save state.
    /// </summary>
    public class SaveStateEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveStateEventArgs"/> class.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        public SaveStateEventArgs(IDictionary<string, object> pageState)
            : base()
        {
            this.PageState = pageState;
        }

        /// <summary>
        /// Gets the state of the page.
        /// </summary>
        /// <value>
        /// An empty dictionary to be populated with serializable state.
        /// </value>
        public IDictionary<string, object> PageState { get; private set; }
    }
}