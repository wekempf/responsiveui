﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Route information.
    /// </summary>
    public sealed partial class RouteInfo
    {
        private readonly Type viewType;

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteInfo"/> class.
        /// </summary>
        /// <param name="viewType">Type of the view.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        public RouteInfo(Type viewType, Type viewModelType)
            : this(viewModelType)
        {
            this.viewType = viewType;
        }

        /// <summary>
        /// Gets the type of the view.
        /// </summary>
        /// <value>
        /// The type of the view.
        /// </value>
        public Type ViewType
        {
            get { return this.viewType; }
        }
    }
}
