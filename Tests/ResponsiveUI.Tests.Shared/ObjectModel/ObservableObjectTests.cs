﻿#if RESPONSIVE_UNIVERSAL

using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

#else

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endif

using System;
using System.Collections.Generic;
using System.Text;

namespace Responsive.ObjectModel
{
    [TestClass]
    public class ObservableObjectTests
    {
        [TestMethod]
        public void PropertyChanged_WhenPropertyChanged_ShouldBeRaised()
        {
            var wasRaised = false;
            var model = new Model();
            model.PropertyChanged += (s, e) => wasRaised = e.PropertyName == "FirstName";
            
            model.FirstName = "Jane";

            Assert.IsTrue(wasRaised);
        }

        [TestMethod]
        public void PropertyChanged_WhenAllPropertiesChanged_ShouldBeRaised()
        {
            var wasRaised = false;
            var model = new Model();
            model.PropertyChanged += (s, e) => wasRaised = string.IsNullOrEmpty(e.PropertyName);

            model.Reset();

            Assert.IsTrue(wasRaised);
        }

        private class Model : ObservableObject
        {
            private string firstName;
            private string lastName;

            public Model()
            {
                this.firstName = "John";
                this.lastName = "Doe";
            }

            public string FirstName
            {
                get { return this.firstName; }
                set { this.SetValue(ref this.firstName, value); }
            }

            public string LastName
            {
                get { return this.lastName; }
                set { this.SetValue(ref this.lastName, value); }
            }

            public void Reset()
            {
                this.firstName = "John";
                this.lastName = "Doe";
                this.OnPropertyChanged(null);
            }
        }
    }
}
