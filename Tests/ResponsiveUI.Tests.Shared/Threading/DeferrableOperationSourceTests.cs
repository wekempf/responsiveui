﻿#if RESPONSIVE_UNIVERSAL
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
#else
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endif

using System;
using System.Threading.Tasks;
using System.Threading;

namespace Responsive.Threading
{
    [TestClass]
    public class DeferrableOperationSourceTests
    {
        [TestMethod]
        public async Task WaitAsync_GivenCancelledToken_ShouldBeCancelled()
        {
            var source = new DeferrableOperationSource();
            var cts = new CancellationTokenSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            cts.Cancel();
            Exception exception = null;

            try
            {
                await source.WaitAsync(cts.Token);
            }
            catch (OperationCanceledException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public async Task WaitAsync_GivenLongEnoughTimeout_ShouldWaitForAllDeferrals()
        {
            var source = new DeferrableOperationSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            var result = await source.WaitAsync(TimeSpan.FromMilliseconds(500));

            Assert.IsTrue(result);
            Assert.AreEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task WaitAsync_GivenLongEnoughTimeoutInMilliseconds_ShouldWaitForAllDeferrals()
        {
            var source = new DeferrableOperationSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            var result = await source.WaitAsync(500);

            Assert.IsTrue(result);
            Assert.AreEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task WaitAsync_GivenShortEnoughTimeout_ShouldTimeout()
        {
            var source = new DeferrableOperationSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            var result = await source.WaitAsync(TimeSpan.FromMilliseconds(10));

            Assert.IsFalse(result);
            Assert.AreNotEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task WaitAsync_GivenShortEnoughTimeoutInMilliseconds_ShouldTimeout()
        {
            var source = new DeferrableOperationSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            var result = await source.WaitAsync(10);

            Assert.IsFalse(result);
            Assert.AreNotEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task WaitAsync_GivenTimeoutAndCancelledToken_ShouldBeCancelled()
        {
            var source = new DeferrableOperationSource();
            var cts = new CancellationTokenSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            cts.Cancel();
            Exception exception = null;

            try
            {
                await source.WaitAsync(TimeSpan.FromMilliseconds(500), cts.Token);
            }
            catch (OperationCanceledException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public async Task WaitAsync_GivenTimeoutInMillisecondsAndCancelledToken_ShouldBeCancelled()
        {
            var source = new DeferrableOperationSource();
            var cts = new CancellationTokenSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            cts.Cancel();
            Exception exception = null;

            try
            {
                await source.WaitAsync(500, cts.Token);
            }
            catch (OperationCanceledException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public async Task WaitAsync_ShouldWaitForAllDeferrals()
        {
            var source = new DeferrableOperationSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(source.DeferrableOperation, counter);
            }

            await source.WaitAsync();

            Assert.AreEqual(10, counter.Count);
        }

        private async void Defer(DeferrableOperation operation, Counter counter)
        {
            using (operation.GetDeferral())
            {
                await Task.Delay(40);
                counter.Increment();
            }
        }
    }
}