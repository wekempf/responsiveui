﻿#if RESPONSIVE_UNIVERSAL
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
#else
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endif

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Responsive.Threading
{
    [TestClass]
    public class AsyncCoordinatorTests
    {
        [TestMethod]
        public async Task ShutdownAsync_ShouldWaitForAllDeferrals()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            await coordinator.ShutdownAsync();

            Assert.AreEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task ShutdownAsync_ShouldCancel()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            await coordinator.ShutdownAsync();

            Assert.IsTrue(coordinator.CancellationToken.IsCancellationRequested);
        }

        [TestMethod]
        public async Task ShutdownAsync_GivenLongEnoughTimeoutInMilliseconds_ShouldWaitForAllDeferrals()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            var result = await coordinator.ShutdownAsync(500);

            Assert.IsTrue(result);
            Assert.AreEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task ShutdownAsync_GivenShortEnoughTimeoutInMilliseconds_ShouldTimeout()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            var result = await coordinator.ShutdownAsync(10);

            Assert.IsFalse(result);
            Assert.AreNotEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task ShutdownAsync_GivenLongEnoughTimeout_ShouldWaitForAllDeferrals()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            var result = await coordinator.ShutdownAsync(TimeSpan.FromMilliseconds(500));

            Assert.IsTrue(result);
            Assert.AreEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task ShutdownAsync_GivenShortEnoughTimeout_ShouldTimeout()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                this.Defer(coordinator, counter);
            }

            var result = await coordinator.ShutdownAsync(TimeSpan.FromMilliseconds(10));

            Assert.IsFalse(result);
            Assert.AreNotEqual(10, counter.Count);
        }

        [TestMethod]
        public async Task ShutdownAsync_GivenCancelledToken_ShouldBeCancelled()
        {
            var coordinator = new AsyncCoordinator();
            var cts = new CancellationTokenSource();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                Defer(coordinator, counter);
            }

            cts.Cancel();
            Exception exception = null;

            try
            {
                await coordinator.ShutdownAsync(cts.Token);
            }
            catch (OperationCanceledException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
        }

        [TestMethod]
        public async Task AddTask_WhenShutdown_ShouldWaitForTask()
        {
            var coordinator = new AsyncCoordinator();
            var counter = new Counter();
            for (int i = 0; i < 10; ++i)
            {
                coordinator.AddTask(IncrementAsync(counter));
            }

            await coordinator.ShutdownAsync();

            Assert.AreEqual(10, counter.Count);
        }

        private async void Defer(AsyncCoordinator coordinator, Counter counter)
        {
            using (coordinator.GetDeferral())
            {
                await IncrementAsync(counter);
            }
        }

        private async Task IncrementAsync(Counter counter)
        {
            await Task.Delay(40);
            counter.Increment();
        }
    }
}
