﻿using System.Threading;

namespace Responsive.Threading
{
    internal class Counter
    {
        private int count;

        public int Count
        {
            get { return this.count; }
        }

        public void Increment()
        {
            Interlocked.Increment(ref this.count);
        }
    }
}