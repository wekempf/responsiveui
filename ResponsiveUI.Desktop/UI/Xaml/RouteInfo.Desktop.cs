﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Responsive.UI.Xaml
{
    /// <content>
    /// Route information content documentation.
    /// </content>
    public sealed partial class RouteInfo
    {
        private readonly Uri viewUri;

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteInfo"/> class.
        /// </summary>
        /// <param name="viewUri">The view URI.</param>
        /// <param name="viewModelType">Type of the view model.</param>
        public RouteInfo(Uri viewUri, Type viewModelType)
            : this(viewModelType)
        {
            this.viewUri = viewUri;
        }

        /// <summary>
        /// Gets the view URI.
        /// </summary>
        /// <value>
        /// The view URI.
        /// </value>
        public Uri ViewUri
        {
            get { return this.viewUri; }
        }
    }
}
