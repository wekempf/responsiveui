﻿using System.Diagnostics.Contracts;

namespace Responsive
{
    internal static class Assumptions
    {
        public static T AssumeNotNull<T>(this T source)
            where T : class
        {
            Contract.Ensures(source != null);
            Contract.Assume(source != null);
            return source;
        }
    }
}