﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Responsive.ObjectModel
{
    /// <summary>
    /// Defines a base class for object types that can be observed for property changes.
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        /// <remarks>
        /// The PropertyChanged event can indicate all properties on the object have changed by using either
        /// <see langword="null"/> or <see cref="String.Empty"/> as the property name in the 
        /// <see cref="PropertyChangedEventArgs"/>.
        /// </remarks>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sets a backing field for a property, raising the <see cref="INotifyPropertyChanged.PropertyChanged"/>
        /// event if the value changes.
        /// </summary>
        /// <typeparam name="T">The backing field type.</typeparam>
        /// <param name="backingField">The backing field to set.</param>
        /// <param name="value">The value to set the backing field to.</param>
        /// <param name="propertyName">The name of the property associated with the backing field.</param>
        /// <returns><see langword="true"/> if the <paramref name="backingField"/> was changed; otherwise,
        /// <see langword="false"/>.</returns>
        protected bool SetValue<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingField, value))
            {
                return false;
            }

            backingField = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        /// <summary>
        /// Called when a property is changed.
        /// </summary>
        /// <param name="propertyName">The name of the property or <see langword="null"/>
        /// or <see cref="String.Empty"/> if all properties where changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
