﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Responsive.ObjectModel
{
    /// <summary>
    /// The Responsive.ObjectModel namespace provides classes and interfaces used to implement models and view models.
    /// </summary>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}
