﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Responsive.Threading.Tasks
{
    /// <summary>
    /// The Responsive.Threading.Tasks namespace provides types that simplify the work of writing concurrent and
    /// asynchronous code.
    /// </summary>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}
