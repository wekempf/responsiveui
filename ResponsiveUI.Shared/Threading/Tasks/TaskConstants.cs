﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading.Tasks;

namespace Responsive.Threading.Tasks
{
    /// <summary>
    /// Provides access to completed Task instances.
    /// </summary>
    public static class TaskConstants
    {
        private static readonly Task<bool> TrueTask = Task.FromResult(true);
        private static readonly Task<bool> FalseTask = Task.FromResult(false);

        /// <summary>
        /// Gets a completed Task.
        /// </summary>
        public static Task Completed
        {
            get
            {
                Contract.Ensures(Contract.Result<Task>() != null);

                return TaskConstants.True;
            }
        }

        /// <summary>
        /// Gets a Task that completed with a <see langword="true"/> result.
        /// </summary>
        public static Task<bool> True
        {
            get
            {
                Contract.Ensures(Contract.Result<Task>() != null);

                return TaskConstants.TrueTask;
            }
        }

        /// <summary>
        /// Gets a Task that completed with a <see langword="false"/> result.
        /// </summary>
        public static Task<bool> False
        {
            get
            {
                Contract.Ensures(Contract.Result<Task>() != null);

                return TaskConstants.FalseTask;
            }
        }
    }
}
