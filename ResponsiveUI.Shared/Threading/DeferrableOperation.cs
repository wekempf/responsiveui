﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Responsive.Threading
{
    /// <summary>
    /// Provides functionality to defer an operation.
    /// </summary>
    public sealed class DeferrableOperation
    {
        private readonly TaskCompletionSource<bool> taskCompletionSource = new TaskCompletionSource<bool>();
        private int count = 1;
        private int waitCount = 1;

        internal DeferrableOperation()
        {
        }

        /// <summary>
        /// Requests that the operation be delayed.
        /// </summary>
        /// <returns>The operation deferral.</returns>
        public Deferral GetDeferral()
        {
            Contract.Ensures(Contract.Result<Deferral>() != null);

            if (Interlocked.CompareExchange(ref this.waitCount, 0, 0) == 0)
            {
                throw new InvalidOperationException("The DeferrableOperation has already been waited on.");
            }

            Interlocked.Increment(ref this.count);
            return new Deferral(this);
        }

        internal void Release()
        {
            if (Interlocked.Decrement(ref this.count) == 0)
            {
                this.taskCompletionSource.TrySetResult(true);
            }
        }

        internal async Task<bool> WaitAsync(int millisecondsTimeout, CancellationToken cancellationToken)
        {
            Contract.Requires(millisecondsTimeout >= -1);

            if (Interlocked.Decrement(ref this.waitCount) == 0)
            {
                this.Release();
            }

            var task = this.taskCompletionSource.Task;
            if (task.IsCompleted)
            {
                return true;
            }

            using (var timeoutTokenSource = new CancellationTokenSource())
            {
                timeoutTokenSource.CancelAfter(millisecondsTimeout);
                using (var linkedTokenSource =
                    CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, timeoutTokenSource.Token))
                {
                    try
                    {
                        await task.ContinueWith(_ => { }, linkedTokenSource.Token);
                        return true;
                    }
                    catch (OperationCanceledException)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            throw;
                        }

                        return false;
                    }
                }
            }
        }

        [ContractInvariantMethod]
        private void ObjectInvariants()
        {
            Contract.Invariant(this.taskCompletionSource != null);
        }
    }
}