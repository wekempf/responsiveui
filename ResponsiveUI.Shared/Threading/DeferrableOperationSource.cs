﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Responsive.Threading.Tasks;

namespace Responsive.Threading
{
    /// <summary>
    /// Provides the functionality to wait for an operation which was deferred to complete.
    /// </summary>
    public sealed class DeferrableOperationSource
    {
        private readonly Lazy<DeferrableOperation> operation = new Lazy<DeferrableOperation>(() => new DeferrableOperation());

        /// <summary>
        /// Gets the <see cref="DeferrableOperation"/> associated with this <see cref="DeferrableOperationSource"/>.
        /// </summary>
        public DeferrableOperation DeferrableOperation
        {
            get
            {
                Contract.Ensures(Contract.Result<DeferrableOperation>() != null);

                var result = this.operation.Value;
                Contract.Assume(result != null);
                return result;
            }
        }

        private bool IsDeferrableOperationCreated
        {
            get { return this.operation.IsValueCreated; }
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will complete when all <see cref="Deferral"/> objects complete.</returns>
        public Task WaitAsync()
        {
            Contract.Ensures(Contract.Result<Task>() != null);

            return this.WaitAsync(-1, CancellationToken.None);
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete, while observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete when all <see cref="Deferral"/> objects complete.</returns>
        public Task WaitAsync(CancellationToken cancellationToken)
        {
            Contract.Ensures(Contract.Result<Task>() != null);

            return this.WaitAsync(-1, cancellationToken);
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete, using a <see cref="TimeSpan"/> to measure the timeout
        /// interval.
        /// </summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait,
        /// or a <see cref="TimeSpan"/> that represents -1 milliseconds to wait indefinitely.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// <see cref="Deferral"/> objects complete within the specified time interval, otherwise with a
        /// result of <see langword="false"/>.</returns>
        public Task<bool> WaitAsync(TimeSpan timeout)
        {
            Contract.Requires(timeout.TotalMilliseconds >= -1 && timeout.TotalMilliseconds < int.MaxValue);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            int milliseconds = (int)timeout.TotalMilliseconds;
            return this.WaitAsync(milliseconds, CancellationToken.None);
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete, using a 32-bit signed integer to measure the timeout
        /// interval.
        /// </summary>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="Timeout.Infinite"/> (-1) to
        /// wait indefinitely.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// <see cref="Deferral"/> objects complete within the specified time interval, otherwise with a
        /// result of <see langword="false"/>.</returns>
        public Task<bool> WaitAsync(int millisecondsTimeout)
        {
            Contract.Requires(millisecondsTimeout >= -1);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            return this.WaitAsync(millisecondsTimeout, CancellationToken.None);
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete, using a <see cref="TimeSpan"/> to measure the timeout
        /// interval, while observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait,
        /// or a <see cref="TimeSpan"/> that represents -1 milliseconds to wait indefinitely.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// <see cref="Deferral"/> objects complete within the specified time interval, otherwise with a
        /// result of <see langword="false"/>.</returns>
        public Task<bool> WaitAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Contract.Requires(timeout.TotalMilliseconds >= -1 && timeout.TotalMilliseconds < int.MaxValue);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            int milliseconds = (int)timeout.TotalMilliseconds;
            return this.WaitAsync(milliseconds, cancellationToken);
        }

        /// <summary>
        /// Asynchronously waits for all deferrals to complete, using a 32-bit signed integer to measure the timeout
        /// interval, while observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="Timeout.Infinite"/> (-1) to
        /// wait indefinitely.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// <see cref="Deferral"/> objects complete within the specified time interval, otherwise with a
        /// result of <see langword="false"/>.</returns>
        public Task<bool> WaitAsync(int millisecondsTimeout, CancellationToken cancellationToken)
        {
            Contract.Requires(millisecondsTimeout >= -1);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            if (!this.IsDeferrableOperationCreated)
            {
                return TaskConstants.True;
            }

            return this.DeferrableOperation
                .WaitAsync(millisecondsTimeout, cancellationToken)
                .AssumeNotNull();
        }

        [ContractInvariantMethod]
        private void ObjectInvariants()
        {
            Contract.Invariant(this.operation != null);
        }
    }
}