﻿using System;

namespace Responsive.Threading
{
    /// <summary>
    /// Manages a delayed operation.
    /// </summary>
    public sealed class Deferral : IDisposable
    {
        private DeferrableOperation operation;

        internal Deferral(DeferrableOperation operation)
        {
            this.operation = operation;
        }

        /// <summary>
        /// Notifies the caller that the deferred operation is complete.
        /// </summary>
        /// <remarks>
        /// <see cref="Deferral.Complete"/> is a synonym for <see cref="Deferral.Dispose"/>.
        /// </remarks>
        public void Complete()
        {
            this.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>
        /// When the <see cref="Deferral"/> is disposed it is completed.
        /// </remarks>
        public void Dispose()
        {
            if (this.operation != null)
            {
                this.operation.Release();
                this.operation = null;
            }
        }
    }
}