﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Responsive.Threading
{
    /// <summary>
    /// The Responsive.Threading namespace provides classes and interfaces that
    /// enable multithreaded and asynchronous programming.
    /// </summary>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}
