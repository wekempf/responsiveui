﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Responsive.Threading
{
    /// <summary>
    /// Provides functionality to coordinate asynchronous operations.
    /// </summary>
    [SuppressMessage(
        "Microsoft.Design",
        "CA1063:ImplementIDisposableCorrectly",
        Justification = "Following the Disposable Design Principle")]
    public class AsyncCoordinator : IDisposable
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly DeferrableOperationSource deferrableOperationSource = new DeferrableOperationSource();

        /// <summary>
        /// Gets a <see cref="CancellationToken"/> that can be used to observe when the <see cref="AsyncCoordinator"/>
        /// is shutdown.
        /// </summary>
        public CancellationToken CancellationToken
        {
            get { return this.cancellationTokenSource.Token; }
        }

        /// <summary>
        /// Adds a <see cref="Task"/> to be waited on when this <see cref="AsyncCoordinator"/> is shutdown.
        /// </summary>
        /// <param name="task">The task to add.</param>
        public async void AddTask(Task task)
        {
            Contract.Requires(task != null);

            using (this.GetDeferral())
            {
                try
                {
                    await task;
                }
                catch (OperationCanceledException)
                {
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>
        /// When the <see cref="Deferral"/> is disposed it is completed.
        /// </remarks>
        [SuppressMessage(
            "Microsoft.Design",
            "CA1063:ImplementIDisposableCorrectly",
            Justification = "Following the Disposable Design Principle.")]
        public void Dispose()
        {
            this.cancellationTokenSource.Dispose();
        }

        /// <summary>
        /// Requests that the shutdown operation be delayed.
        /// </summary>
        /// <returns>The operation deferral.</returns>
        public Deferral GetDeferral()
        {
            Contract.Ensures(Contract.Result<Deferral>() != null);

            if (this.CancellationToken.IsCancellationRequested)
            {
                throw new InvalidOperationException("The AsyncCoordinator is already shutdown or is being shutdown.");
            }

            return this.deferrableOperationSource.DeferrableOperation.GetDeferral();
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>.
        /// </summary>
        /// <returns>A <see cref="Task"/> that will complete when all associated <see cref="Task"/> objects complete
        /// and all <see cref="Deferral"/> objects complete.</returns>
        public Task ShutdownAsync()
        {
            Contract.Ensures(Contract.Result<Task>() != null);

            this.cancellationTokenSource.Cancel();
            return this.deferrableOperationSource.WaitAsync();
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>, while observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete when all associated <see cref="Task"/> objects complete
        /// and all <see cref="Deferral"/> objects complete.</returns>
        public Task ShutdownAsync(CancellationToken cancellationToken)
        {
            Contract.Ensures(Contract.Result<Task>() != null);
            return this.ShutdownAsyncImpl(cancellationToken).AssumeNotNull();
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>, using a <see cref="TimeSpan"/> to measure timeout.
        /// </summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait,
        /// or a <see cref="TimeSpan"/> that represents -1 milliseconds to wait indefinitely.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// associated <see cref="Task"/> objects and all <see cref="Deferral"/> objects complete within the specified
        /// time interval, otherwise with a result of <see langword="false"/>.</returns>
        public Task<bool> ShutdownAsync(TimeSpan timeout)
        {
            Contract.Requires(timeout.TotalMilliseconds >= -1 && timeout.TotalMilliseconds < int.MaxValue);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            this.cancellationTokenSource.Cancel();
            return this.deferrableOperationSource.WaitAsync(timeout);
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>, using a 32-bit signed integer to measure timeout.
        /// </summary>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="Timeout.Infinite"/> (-1) to
        /// wait indefinitely.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// associated <see cref="Task"/> objects and all <see cref="Deferral"/> objects complete within the specified
        /// time interval, otherwise with a result of <see langword="false"/>.</returns>
        public Task<bool> ShutdownAsync(int millisecondsTimeout)
        {
            Contract.Requires(millisecondsTimeout >= -1);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            this.cancellationTokenSource.Cancel();
            return this.deferrableOperationSource.WaitAsync(millisecondsTimeout);
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>, using a <see cref="TimeSpan"/> to measure timeout, while
        /// observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="timeout">A <see cref="TimeSpan"/> that represents the number of milliseconds to wait,
        /// or a <see cref="TimeSpan"/> that represents -1 milliseconds to wait indefinitely.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// associated <see cref="Task"/> objects and all <see cref="Deferral"/> objects complete within the specified
        /// time interval, otherwise with a result of <see langword="false"/>.</returns>
        public Task<bool> ShutdownAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Contract.Requires(timeout.TotalMilliseconds >= -1 && timeout.TotalMilliseconds < int.MaxValue);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            this.cancellationTokenSource.Cancel();
            return this.deferrableOperationSource.WaitAsync(timeout, cancellationToken);
        }

        /// <summary>
        /// Shuts down this <see cref="AsyncCoordinator"/>, using a 32-bit signed integer to measure timeout, while
        /// observing a <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="Timeout.Infinite"/> (-1) to
        /// wait indefinitely.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> to observe.</param>
        /// <returns>A <see cref="Task"/> that will complete with a result of <see langword="true"/> if all
        /// associated <see cref="Task"/> objects and all <see cref="Deferral"/> objects complete within the specified
        /// time interval, otherwise with a result of <see langword="false"/>.</returns>
        public Task<bool> ShutdownAsync(int millisecondsTimeout, CancellationToken cancellationToken)
        {
            Contract.Requires(millisecondsTimeout >= -1);
            Contract.Ensures(Contract.Result<Task<bool>>() != null);

            this.cancellationTokenSource.Cancel();
            return this.deferrableOperationSource.WaitAsync(millisecondsTimeout, cancellationToken);
        }

        [ContractInvariantMethod]
        private void ObjectInvariants()
        {
            Contract.Invariant(this.cancellationTokenSource != null);
            Contract.Invariant(this.deferrableOperationSource != null);
        }

        private async Task ShutdownAsyncImpl(CancellationToken cancellationToken)
        {
            this.cancellationTokenSource.Cancel();
            await this.deferrableOperationSource.WaitAsync(cancellationToken);
        }
    }
}