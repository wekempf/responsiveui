﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Route information.
    /// </summary>
    public sealed partial class RouteInfo
    {
        private readonly Type viewModelType;

        private RouteInfo(Type viewModelType)
        {
            this.viewModelType = viewModelType;
        }

        /// <summary>
        /// Gets the type of the view model.
        /// </summary>
        /// <value>
        /// The type of the view model.
        /// </value>
        public Type ViewModelType
        {
            get { return this.viewModelType; }
        }
    }
}
