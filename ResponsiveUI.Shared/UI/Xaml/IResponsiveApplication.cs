﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Responsive.Threading;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// Responsive application.
    /// </summary>
    [ContractClass(typeof(IResponsiveApplicationContract))]
    public interface IResponsiveApplication
    {
        /// <summary>
        /// Gets a <see cref="CancellationToken"/> that can be used to observe when the <see cref="AsyncCoordinator"/>
        /// is shutdown.
        /// </summary>
        CancellationToken CancellationToken { get; }

        /// <summary>
        /// Adds a <see cref="Task"/> to be waited on when this <see cref="AsyncCoordinator"/> is shutdown.
        /// </summary>
        /// <param name="task">The task to add.</param>
        void AddTask(Task task);

        /// <summary>
        /// Requests that the shutdown operation be delayed.
        /// </summary>
        /// <returns>The operation deferral.</returns>
        Deferral GetDeferral();
    }
}
