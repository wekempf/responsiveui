﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Responsive.UI.Xaml
{
    /// <summary>
    /// The Responsive.UI.Xaml namespace provides types used to create UI applications.
    /// </summary>
    [CompilerGenerated]
    internal class NamespaceDoc
    {
    }
}
