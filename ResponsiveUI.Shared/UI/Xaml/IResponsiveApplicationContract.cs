﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Responsive.Threading;

namespace Responsive.UI.Xaml
{
    [ContractClassFor(typeof(IResponsiveApplication))]
    internal abstract class IResponsiveApplicationContract : IResponsiveApplication
    {
        public CancellationToken CancellationToken
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void AddTask(Task task)
        {
            Contract.Requires(task != null);
            throw new NotImplementedException();
        }

        public Deferral GetDeferral()
        {
            Contract.Ensures(Contract.Result<Deferral>() != null);
            throw new NotImplementedException();
        }
    }
}
